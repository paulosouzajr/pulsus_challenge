# Challenge Pulsus

Folder sequentialSum contains the first problem. Linear solution were developed to ensure the result from the recursive proposal (i.e. recursiveSubsetSum).

Folder sequentialChar contains the second problem solutions.

Besides, *_test.py provide tests units to validate the outputs.


To run:

python3 file_name.py