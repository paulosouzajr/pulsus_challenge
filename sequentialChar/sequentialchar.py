# Linear solution to find the most frequent char in a sequence
def findMostRepeat(line):
    # Init counters and "buffers"
    count = 0
    oldCounter = 0
    output = line[0]
    old_output = ""

    # Iterates over the entire string
    for i in range(0, len(line) - 1):
        # if next char is equal the actual: concat char and increase counter
        if line[i] == line[i + 1]:
            count += 1
            output += line[i + 1]
        else:
            # if not equal: test if the actual result is greater then the last one
            if count > oldCounter:
                oldCounter = count
                old_output = output
            # if not greater wipes the counter and buffer
            count = 1
            output = line[i + 1]
    # garantees that the final found output is the greater one
    if count > oldCounter:
        oldCounter = count
        old_output = output
    return old_output


if __name__ == '__main__':
    stri = "Preeeeeeeeeeeaaaaaaoooooooooooooooooooooooooooooooooooooooooooooooo"

    print(findMostRepeat(stri))
