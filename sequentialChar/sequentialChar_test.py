import unittest

import sequentialChar


class TestSequentialChar(unittest.TestCase):
    def test_common(self):
        stri = "Pressaaao"
        expected = "aaa"
        self.assertEqual(expected,
                         sequentialChar.findMostRepeat(stri))

    def test_start(self):
        stri = "Ppppppppppppppreeeeessao"
        expected = "ppppppppppppp"
        self.assertEqual(expected,
                         sequentialChar.findMostRepeat(stri))

    def test_middle(self):
        stri = "Preeeeeeeeeeeeeeeeessssssssssssssssaaao"
        expected = "eeeeeeeeeeeeeeeee"
        self.assertEqual(expected,
                         sequentialChar.findMostRepeat(stri))

    def test_end(self):
        stri = "Pressaaaaaoooooo"
        expected = "oooooo"
        self.assertEqual(expected,
                         sequentialChar.findMostRepeat(stri))


if __name__ == '__main__':
    unittest.main()
