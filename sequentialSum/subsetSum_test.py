import random
import unittest

import linear
import recursiveSubsetSum


class TestMaxSubsetSum(unittest.TestCase):
    def test_common(self):
        someList = [2, -4, 6, 8, -10, 100, 0, -6, 5]
        expected = [104, 2, 5]
        self.assertEqual(expected,
                         linear.subsetSum(someList, 0, len(someList) - 1))
        self.assertEqual(expected,
                         recursiveSubsetSum.divideProblem(someList, 0, len(someList) - 1))
        self.assertEqual(linear.subsetSum(someList, 0, len(someList) - 1),
                         recursiveSubsetSum.divideProblem(someList, 0, len(someList) - 1))

    def test_random(self):
        randomList = random.sample(range(-50000, 50000), 100000)
        self.assertEqual(linear.subsetSum(randomList, 0, len(randomList) - 1),
                         recursiveSubsetSum.divideProblem(randomList, 0, len(randomList) - 1))

    def test_zeros(self):
        zeros = [0, 0, 0, 0, 0]
        self.assertEqual(linear.subsetSum(zeros, 0, len(zeros) - 1),
                         recursiveSubsetSum.divideProblem(zeros, 0, len(zeros) - 1))

    def test_oddMiddle(self):
        odd = [-5, -1, 6, -2, 5, -5, 2]
        self.assertEqual(linear.subsetSum(odd, 0, len(odd) - 1)[0],
                         recursiveSubsetSum.divideProblem(odd, 0, len(odd) - 1)[0])

    def test_none(self):
        odd = [0, -1, 0, -1, 0, -1]
        self.assertEqual(linear.subsetSum(odd, 0, len(odd) - 1),
                         recursiveSubsetSum.divideProblem(odd, 0, len(odd) - 1))


if __name__ == '__main__':
    unittest.main()


    # "Might exist results in Linear that differ from Recursive solutions considering that may exist multiple valid results."
