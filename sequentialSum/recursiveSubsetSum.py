# Sum the subsets in order to determine which is the largest sum
# returns a list with the following format [sum, initial_position, final_position] of the subset
def subsetsum(conj, start, end, half):
    add = auxL = auxR = posI = posE = 0
    for i in range(half, start, -1):
        add += conj[i]
        if add > auxL:
            auxL = add
            posI = i
    add = 0
    for i in range(half + 1, end + 1):
        add += conj[i]
        if add > auxR:
            auxR = add
            posE = i

    return [auxL + auxR, posI, posE]


# Recursive function that divide the set to be processed
def divideProblem(conj, start, end):
    if (end == start):
        return [conj[start], start, end]

    # print("First level of recursion: ", start, end)

    offset = int((start + end) / 2)

    return bigger(bigger(divideProblem(conj, start, offset), divideProblem(conj, offset + 1, end)),
                  subsetsum(conj, start, end, offset))


# Receives two of possible subsets and returns the subset which possesses the larger sum
def bigger(a, b):
    return a if (a[0] > b[0]) else b


if __name__ == '__main__':
    # Init a list
    myList = [2, -4, 6, 8, -10, 100, 0, -6, 5]

    res = divideProblem(myList, 0, len(myList) - 1)

    # Print the first and final position of subset that has the maximum sum
    print(res[1], res[2])
