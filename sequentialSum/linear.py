# Linear solution created in order to validate the results of the recursive function
# Iterates over the entire vector optimistically looking for the more significant sum,
# until it found another fittable solution that overlaps the previous one.
def subsetSum(conj, start, end):
    newSum = lastSum = startAt = beginPos = endPos = 0
    for i in range(start, end):
        newSum += conj[i]
        if newSum < 0:
            startAt = i + 1
            newSum = 0
        elif newSum > lastSum:
            lastSum = newSum
            beginPos = startAt
            endPos = i

    return [lastSum, beginPos, endPos]


if __name__ == '__main__':
    myList = [2, -4, 6, 8, -10, 100, 0, -6, 5]

    res = subsetSum(myList, 0, len(myList) - 1)

    print(res[1], res[2])
